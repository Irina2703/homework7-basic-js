"use strict"
// 1. Перевірити, чи є рядок паліндромом.Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
//     (читається однаково зліва направо і справа наліво), або false в іншому випадку.




function isPalindrome (str) {
    str = str.toLowerCase();
    return str === str.split('').reverse().join('');
    
}

console.log(isPalindrome("Anna"));



// 2. Створіть функцію, яка  перевіряє довжину рядка.Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший.Ця функція стане в нагоді для валідації форми.Приклади використання функції:
// // Рядок коротше 20 символів
// funcName('checked string', 20); // true
// // Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false


let str;
function stringLength(str, maxLength = 20) {
    if (str.length <= maxLength) {
        return true
    }
    return false;
}
console.log(stringLength("Hello world"));



// 3. Створіть функцію, яка визначає скільки повних років користувачу.
//    Отримайте дату народження користувача через prompt.
//    Функція повина повертати значення повних років на дату виклику функцію.



const age = message => {
    let userAge = prompt(message);
    const now = new Date();
    let userBirthdayDate = new Date(userAge)
   

    let age = now.getFullYear() - userBirthdayDate.getFullYear();
    let month = now.getMonth() - userBirthdayDate.getMonth();
    if (month < 0 || (month === 0 && now.getDate() < userBirthdayDate.getDate())) {
        age--;
    } return age;
}

const userAge = age("Enter your date of birthday YYYY-MM-DD")
console.log(userAge);
// console.log(now);




